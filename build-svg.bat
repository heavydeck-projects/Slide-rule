@echo off
set SCAD=C:\Program Files\OpenSCAD\openscad.exe
set OUT_FORMAT=svg

md output

echo -------------------------------
echo --- Rendering SVG documents ---
echo -------------------------------
rem generate the markings
echo. | "%SCAD%" -o output/slide-markings.%OUT_FORMAT% -Dshow_fiducials=true -Dshow_outlines=false -Dshow_base=false -Dshow_slide_scale=true -Dshow_bottom_scale=false -Dshow_top_scale=false -Dshow_engraving=true slide-rule.scad
echo. | "%SCAD%" -o output/fixed-markings.%OUT_FORMAT% -Dshow_fiducials=true -Dshow_outlines=false -Dshow_base=false -Dshow_slide_scale=false -Dshow_bottom_scale=true -Dshow_top_scale=true -Dshow_engraving=true slide-rule.scad
rem generate the scales
echo. | "%SCAD%" -o output/slide-rule.%OUT_FORMAT% -Dshow_fiducials=true -Dshow_outlines=true -Dshow_base=false -Dshow_slide_scale=true -Dshow_bottom_scale=false -Dshow_top_scale=false -Dshow_engraving=false slide-rule.scad
echo. | "%SCAD%" -o output/fixed-rule.%OUT_FORMAT% -Dshow_fiducials=true -Dshow_outlines=true -Dshow_base=false -Dshow_slide_scale=false -Dshow_bottom_scale=true -Dshow_top_scale=true -Dshow_engraving=false slide-rule.scad
rem generate cursor
echo. | "%SCAD%" -o output/cursor-markings.%OUT_FORMAT% -Dshow_fiducials=true -Dshow_outlines=false -Dshow_engraving=true cursor.scad
echo. | "%SCAD%" -o output/cursor-rule.%OUT_FORMAT% -Dshow_fiducials=true -Dshow_outlines=true -Dshow_engraving=false cursor.scad

echo --------------------------------
echo --- Full rule PNG renderings ---
echo --------------------------------
rem Sample PNG render
echo. | "%SCAD%" -o output/full-1900-010.png --imgsize 1900,340 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.10 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-1900-015.png --imgsize 1900,340 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.15 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-1900-020.png --imgsize 1900,340 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.20 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-1900-025.png --imgsize 1900,340 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.25 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-1900-030.png --imgsize 1900,340 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.30 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-1900-035.png --imgsize 1900,340 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.35 -Dslide_x_offset=1 slide-rule.scad

echo --- 2x resolution ---
echo. | "%SCAD%" -o output/full-3800-010.png --imgsize 3800,680 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.10 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-3800-015.png --imgsize 3800,680 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.15 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-3800-020.png --imgsize 3800,680 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.20 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-3800-025.png --imgsize 3800,680 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.25 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-3800-030.png --imgsize 3800,680 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.30 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-3800-035.png --imgsize 3800,680 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.35 -Dslide_x_offset=1 slide-rule.scad

echo --- 4x resolution ---
echo. | "%SCAD%" -o output/full-7600-010.png --imgsize 7600,1360 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.10 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-7600-015.png --imgsize 7600,1360 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.15 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-7600-020.png --imgsize 7600,1360 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.20 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-7600-025.png --imgsize 7600,1360 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.25 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-7600-030.png --imgsize 7600,1360 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.30 -Dslide_x_offset=1 slide-rule.scad
echo. | "%SCAD%" -o output/full-7600-035.png --imgsize 7600,1360 --camera 175,26.4,0,0,0,0,150 --projection=o -Dline_w=0.35 -Dslide_x_offset=1 slide-rule.scad