
default_font="Liberation Sans";

module notch(w,h){
    translate([-(w/2),0,0]) square([w,h]);
}

//Draws a one-decade log-scale. Divisions include:
// - 0.5  marks on all the range
// - 0.1  marks on all the range
// - 0.02 marks on 1-2 range
// - 0.05 marks on 2-5 range
module decade(width, notch_width=0.1, notch_height=5){
    
    h_figure  = notch_height;
    h_halfway = 0.75 * h_figure;
    h_tenths  = 0.5  * h_figure;
    h_tiny    = 0.3  * h_figure;
    
    //union()
    {
        //Draw the "big" notches
        for(i = [1:10]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_figure);
        }
        
        //Draw the "half" notches
        for(i = [1.5:10]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_halfway);
        }
        
        //Draw the tenths
        for(i = [1:0.1:10]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_tenths);
        }
        
        //Draw the 0.02 divisions on the
        //range: 1-2
        for(i = [1:0.02:2]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_tiny);
        }
        
        //Draw the 0.05 divisions on the
        //range: 2-5
        for(i = [2:0.05:5]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_tiny);
        }
    }
    
}

//Similar to regular decade but with extended precission
//and "big" marks on tenths on the 0.1 -> 0.9 range
module decade_precise (width, notch_width=0.1, notch_height=5){
    
    h_figure  = notch_height;
    h_halfway = 0.75 * h_figure;
    h_tenths  = 0.5  * h_figure;
    h_tiny    = 0.3  * h_figure;
    
    //union()
    {
        
        //Draw the "big" notches
        for(i = [1:10]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_figure);
        }
        
        //Draw the "half" notches for x>2
        for(i = [2.5:10]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_halfway);
        }
        
        //Draw the tenths for x>2
        for(i = [2:0.1:10]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_tenths);
        }
        
        //Draw the 0.05 for x>4
        for(i = [4:0.05:10]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_tiny);
        }
        
        //Draw the 0.02 for 2>x>4
        for(i = [2:0.02:4]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_tiny);
        }
        
        // --- "Tiny" [1:0.01:2] range ---
        //Draw the 0.1 "big" notches
        for(i = [1:0.1:2]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_figure);
        }
        
        //Draw the 0.05 marks
        for(i = [1:0.05:2]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_halfway);
        }
        
        //Draw the 0.01 marks
        for(i = [1:0.01:2]){
            translate([log(i)*width,0,0])
                notch(notch_width, h_tenths);
        }
    }
    
}

//Annotates the log-scale
module log_scale_labels(width, text_width=0.1, text_height=2, font=default_font, print_one = false, precise=false, end_value=10, inverse=false){
    
    //union()
    if(!inverse){
        for(i = [2:9]){
            translate([log(i)*width,0,0])
                text(str(i), text_height, font);
        }
        //end_value
        translate([width,0,0])
            text(str(end_value), text_height, font);
        
        if(print_one){
            text(str(1), text_height, font);
        }
        
        if(precise) for(i = [1.1:0.1:1.91]){ //<-- using 1.91 bc of floating point issues
			precise_label = str(".", (i-1)*10);
            translate([log(i)*width,0,0])
                text(precise_label, text_height, font);
        }
    }
    else{
        for(i = [2:9]){
            translate([log(i)*width,0,0])rotate([0,180,0])
                text(str(i), text_height, font);
        }
        //end_value
        translate([width,0,0])rotate([0,180,0])
            text(str(end_value), text_height, font);
        
        if(print_one){
            rotate([0,180,0])
                text(str(1), text_height, font);
        }
        
        if(precise) for(i = [1.1:0.1:1.91]){ //<-- using 1.91 bc of floating point issues
            precise_label = str(".", (i-1)*10);
            translate([log(i)*width,0,0])rotate([0,180,0])
                text(precise_label, text_height, font);
        }
    }
}

// --------------------
// --- Sample usage ---
// --------------------

//Log-scales
translate([10,10,0])union(){
    //Draw a precise 1-decade scale
    translate([0,0.5,0]){
        decade_precise(300);
        translate([0.1,6-2,0]) log_scale_labels(300, print_one=true, precise=true);
    }
    //Draw the complementary scale
    translate([0,0,0]){
        rotate (180, [1,0,0]) decade_precise(300);
        translate([0.1,-6,0]) log_scale_labels(300, print_one=true, precise=true);
    }
    
    //Draw 2-decade scale
    translate([0,20.5,0]){
        {
            decade(300/2);
            translate([0.1,6-2,0]) log_scale_labels(300/2, print_one=true);
        }
        translate([300/2,0,0]){
            decade(300/2);
            translate([0.1,6-2,0]) log_scale_labels(300/2, end_value=100);
        }
    }
    //Draw the complementary scale
    translate([0,20,0]){
        {
            rotate (180, [1,0,0]) decade(300/2);
            translate([0.1,-6,0]) log_scale_labels(300/2, print_one=true);
        }
        translate([300/2,0,0]){
            rotate (180, [1,0,0]) decade(300/2);
            translate([0.1,-6,0]) log_scale_labels(300/2, end_value=100);
        }
    }
}

//Inverse log-scales
translate([10,-35,0])translate([300,0,0]) rotate(180, [0,1,0])union(){
    //Draw a precise 1-decade scale
    translate([0,0.5,0]){
        decade_precise(300);
        translate([0.1,6-2,0]) log_scale_labels(300, print_one=true, precise=true, inverse=true);
    }
    //Draw the complementary scale
    translate([0,0,0]){
        rotate (180, [1,0,0]) decade_precise(300);
        translate([0.1,-6,0]) log_scale_labels(300, print_one=true, precise=true, inverse=true);
    }
    
    //Draw 2-decade scale
    translate([0,20.5,0]){
        {
            decade(300/2);
            translate([0.1,6-2,0]) log_scale_labels(300/2, print_one=true, inverse=true);
        }
        translate([300/2,0,0]){
            decade(300/2);
            translate([0.1,6-2,0]) log_scale_labels(300/2, end_value=100, inverse=true);
        }
    }
    //Draw the complementary scale
    translate([0,20,0]){
        {
            rotate (180, [1,0,0]) decade(300/2);
            translate([0.1,-6,0]) log_scale_labels(300/2, print_one=true, inverse=true);
        }
        translate([300/2,0,0]){
            rotate (180, [1,0,0]) decade(300/2);
            translate([0.1,-6,0]) log_scale_labels(300/2, end_value=100, inverse=true);
        }
    }
}