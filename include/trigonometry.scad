
module notch(w,h){
    translate([-(w/2),0,0]) square([w,h]);
}

//Sine function
// - Origin on asin(0.1)
// - End on 90º
module sine_scale(width, notch_width=0.1, notch_height=5){
    
    h_figure  = notch_height;
    h_halfway = 0.75 * h_figure;
    h_tenths  = 0.5  * h_figure;
    h_tiny    = 0.3  * h_figure;
    
    //union()
    {
        //Draw the 5.5 notch (negative side)
        if(false){ //(drawn by .5 loop)
            translate([log( sin(5.5) * 10) * width, 0, 0]){
                notch(notch_width,h_halfway);
            }
        }
        
        //Draw the 6-20 range (increment of 1)
        for(i = [6:1:20]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the .5 marks on the 5.5 -> 20 range
        for(i = [5.5:1:20]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
        
        //Draw the .1 marks on the 5.5 -> 20 range
        for(i = [55:1:200]){
            translate([log( sin(i/10) * 10 ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
        
        //Draw the 20-40 range (increment of 5)
        for(i = [20:5:40]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the unit marks on the 20 -> 40 range
        for(i = [20:1:40]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
        
        //Draw the .2 marks on the 20 -> 40 range
        for(i = [200:2:400]){
            translate([log( sin(i/10) * 10 ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
        
        //Draw the 40-80 range (increment of 10)
        for(i = [40:10:80]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the 5 marks on the 40 -> 60 range
        for(i = [40:5:80]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
        
        //Draw the 1 marks on the 40 -> 80 range
        for(i = [40:1:80]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
        
        //Draw the .5 marks on the 40 -> 60 range
        for(i = [400:5:600]){
            translate([log( sin(i/10) * 10 ) * width, 0, 0]){
                notch(notch_width, h_tiny);
            }
        }
        
        //Draw the 85º notch
        {
            translate([log( sin(85) * 10 ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
        
        //Draw the 90º notch
        {
            translate([log( sin(90) * 10 ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
    }
}

//Sine function
// - Origin on asin(0.01)
// - End on asin(0.1)
module small_sine_scale(width, notch_width=0.1, notch_height=5){
    
    h_figure  = notch_height;
    h_halfway = 0.75 * h_figure;
    h_tenths  = 0.5  * h_figure;
    h_tiny    = 0.3  * h_figure;
    
    //union()
    {
        //Draw the .55 notch (negative side)
        {
            translate([log( sin(0.55) * 100) * width, 0, 0]){
                notch(notch_width,h_halfway);
            }
        }
        
        //Draw the 0.6-1 range (increment of 0.1)
        for(i = [6:1:10]){
            translate([log( sin(i/10) * 100 ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the .05 marks in range 0.55 -> 1
        for(i = [55:5:100]){
            translate([log( sin(i/100) * 100 ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
        
        //Draw the .01 marks in range 0.55 -> 2
        for(i = [55:1:200]){
            translate([log( sin(i/100) * 100 ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
        
        //Draw the .005 marks in range 0.55 -> 1
        for(i = [550:5:1000]){
            translate([log( sin(i/1000) * 100 ) * width, 0, 0]){
                notch(notch_width, h_tiny);
            }
        }
        
        
        //Draw the 1-2 range (increment of 0.1)
        for(i = [10:1:20]){
            translate([log( sin(i/10) * 100 ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the 2-6 range (increment of 0.5)
        for(i = [20:5:60]){
            translate([log( sin(i/10) * 100 ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the .1 marks in range 2 -> 6
        for(i = [20:1:60]){
            translate([log( sin(i/10) * 100 ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
        
        //Draw the .02 marks in range 2 -> 6
        for(i = [200:2:600]){
            translate([log( sin(i/100) * 100 ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
        
        
        
    }
}

//Tangent function
// - Origin on atan(0.1) -> 0.1
// - End on 45º
module tan1_scale(width, notch_width=0.1, notch_height=5){
    
    h_figure  = notch_height;
    h_halfway = 0.75 * h_figure;
    h_tenths  = 0.5  * h_figure;
    h_tiny    = 0.3  * h_figure;
    
    //union()
    {
        //Draw the 5.5 notch (negative side)
        if(false){
            translate([log( tan(5.5) * 10) * width, 0, 0]){
                notch(notch_width,h_halfway);
            }
        }
        
        //Draw the 6-10 range (increment of 1)
        for(i = [6:1:10]){
            translate([log( tan(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the halfway marks on the 5.5 -> 10 range
        for(i = [5.5:1:10]){
            translate([log( tan(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
        
        //Draw the tenths marks on the 5.5 -> 10 range
        for(i = [55:1:100]){
            translate([log( tan(i/10) * 10 ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
        
        //Draw the 20-45 range (increment of 5)
        for(i = [10:5:45]){
            translate([log( tan(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the unit marks on the 10 -> 45
        for(i = [10:1:45]){
            translate([log( tan(i) * 10 ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
        
        //Draw the 0.2 marks on the 10 -> 45 range
        for(i = [100:2:450]){
            translate([log( tan(i/10) * 10 ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
    }
}

//Tangent function
// - Origin on 45º
// - End on atan(10)
module tan2_scale(width, notch_width=0.1, notch_height=5){
    
    h_figure  = notch_height;
    h_halfway = 0.75 * h_figure;
    h_tenths  = 0.5  * h_figure;
    h_tiny    = 0.3  * h_figure;
    
    //union()
    {
        //Draw the 45-80 range (increment of 5)
        for(i = [45:5:80]){
            translate([log( tan(i) ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the unit marks on the 45 -> 80
        for(i = [45:1:80]){
            translate([log( tan(i) ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
        
        //Draw the 0.2 marks on the 45 -> 60
        for(i = [450:2:600]){
            translate([log( tan(i/10) ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
        
        //Draw the 0.5 marks on the 60 -> 80
        for(i = [600:5:800]){
            translate([log( tan(i/10) ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
        
        //Draw the 0.1 marks on the 60 -> 80
        for(i = [600:1:800]){
            translate([log( tan(i/10) ) * width, 0, 0]){
                notch(notch_width, h_tiny);
            }
        }
        
        //Draw the 80-84 range (increment of 1)
        for(i = [80:1:84]){
            translate([log( tan(i) ) * width, 0, 0]){
                notch(notch_width, h_figure);
            }
        }
        
        //Draw the 0.5 marks on the 80 -> 84.5
        for(i = [800:5:845]){
            translate([log( tan(i/10) ) * width, 0, 0]){
                notch(notch_width, h_tenths);
            }
        }
        
        //Draw the 0.1 marks on the 80 -> 84.5
        for(i = [800:1:845]){
            translate([log( tan(i/10) ) * width, 0, 0]){
                notch(notch_width, h_tiny);
            }
        }
        
        //Draw the 84.5 notch
        {
            translate([log( tan(84.5) ) * width, 0, 0]){
                notch(notch_width, h_halfway);
            }
        }
    }
}

module sine_labels(width, text_height=2, font="Liberation Sans"){
    //union()
    {
        //Draw the 5.5 notch (negative side)
        {
            translate([log( sin(5.5) * 10) * width, 0, 0]){
                    text(str(5.5), text_height, font);
            }
        }
        
        //Draw the 6-20 range (increment of 1)
        for(i = [6:1:20]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                    text(str(i), text_height, font);
            }
        }
        
        //Draw the 20-40 range (increment of 5)
        for(i = [20:5:40]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                    text(str(i), text_height, font);
            }
        }
        
        //Draw the 40-70 range (increment of 10)
        for(i = [40:10:70]){
            translate([log( sin(i) * 10 ) * width, 0, 0]){
                    text(str(i), text_height, font);
            }
        }
        
        //Draw the 90º notch
        {
            translate([log( sin(90) * 10 ) * width, 0, 0]){
                    text(str(90), text_height, font);
            }
        }
    }
}

module small_sine_labels(width, text_height=2, font="Liberation Sans"){
    //union()
    {
        //Draw the .55 notch (negative side)
        {
            translate([log( sin(0.55) * 100) * width, 0, 0]){
                    text(".55", text_height, font);
            }
        }
        
        //Draw the 0.6-1.8 range (increment of 0.1)
        for(i = [6:1:19]){
            translate([log( sin(i/10) * 100 ) * width, 0, 0]){
                text(str(i/10), text_height, font);
            }
        }
        
        //Draw the 2-5.5 range (increment of 0.5)
        for(i = [20:5:55]){
            translate([log( sin(i/10) * 100 ) * width, 0, 0]){
                    text(str(i/10), text_height, font);
            }
        }
    }
}

module cosine_labels(width, text_height=2, font="Liberation Sans:style=Italic"){
    //union()
    {
        //Draw the 20-80 range (increment of 1)
        for(i = [20:10:80]){
            translate([log( cos(i) * 10 ) * width, 0, 0]){
                    text(str(i), text_height, font, halign="right");
            }
        }
        
    }
}

module tan1_labels(width, text_height=2, font="Liberation Sans"){
    //union()
    {
        //Draw the 5.5 notch (negative side)
        {
            translate([log( tan(5.5) * 10) * width, 0, 0]){
                    text(str(5.5), text_height, font);
            }
        }
        
        //Draw the 6-20 range (increment of 1)
        for(i = [6:1:10]){
            translate([log( tan(i) * 10 ) * width, 0, 0]){
                    text(str(i), text_height, font);
            }
        }
        
        //Draw the 20-40 range (increment of 5)
        for(i = [10:5:45]){
            translate([log( tan(i) * 10 ) * width, 0, 0]){
                    text(str(i), text_height, font);
            }
        }
    }
}

module tan2_labels(width, text_height=2, font="Liberation Sans"){
    //union()
    {
        
        //Draw the 45-80 range (increment of 5)
        for(i = [45:5:80]){
            translate([log( tan(i) ) * width, 0, 0]){
                    text(str(i), text_height, font);
            }
        }
        
        //Draw the 80-84 range (increment of 1)
        for(i = [80:1:84]){
            translate([log( tan(i) ) * width, 0, 0]){
                    text(str(i), text_height, font);
            }
        }

        //Draw the 84.5 label
        if(false){
            translate([log( tan(84.5) ) * width, 0, 0]){
                    text(str(84.5), text_height, font);
            }
        }
    }
}

//Tests
translate([0,30,0]){
    sine_scale(300);
    translate([0,5-2,0]) sine_labels(300);
    translate([-0.1,5-2.5,0]) cosine_labels(300);
}

translate([0,20,0]){
    tan1_scale(300);
    translate([0,5-2,0]) tan1_labels(300);
}

translate([0,10,0]){
    tan2_scale(300);
    translate([0,5-2,0]) tan2_labels(300);
}

translate([0,0,0]){
    small_sine_scale(300);
    translate([0,5-2,0]) small_sine_labels(300);
}