
default_font="Liberation Sans";
inch_mm = 25.4;

module notch(w,h){
    translate([-(w/2),0,0]) square([w,h]);
}

module centimeter(width, notch_width=0.1, notch_height=5){
    
    h_figure  = notch_height;
    h_halfway = 0.75 * h_figure;
    h_tenths  = 0.5  * h_figure;
    h_tiny    = 0.3  * h_figure;
    
    //union()
    {
        //Draw the cm notches
        for(i = [0:10:width]){
            translate([i,0,0])
                notch(notch_width, h_figure);
        }
        
        //Draw the .5 notches
        for(i = [0:5:width]){
            translate([i,0,0])
                notch(notch_width, h_halfway);
        }
        
        //Draw the .1 notches
        for(i = [0:1:width]){
            translate([i,0,0])
                notch(notch_width, h_tenths);
        }
        
        //Draw the .05 notches in range 0-3
        for(i = [0:0.5:30]){
            translate([i,0,0])
                notch(notch_width, h_tiny);
        }
    }
}

module inch(width, notch_width=0.1, notch_height=5){
    
    h_figure  = notch_height;
    h_halfway = 0.75 * h_figure;
    h_tenths  = 0.5  * h_figure;
    h_tiny    = 0.3  * h_figure;
    
    //union()
    {
        //Draw the inch notches
        for(i = [0:inch_mm:width]){
            translate([i,0,0])
                notch(notch_width, h_figure);
        }
        
        //Draw the .5 notches
        for(i = [0:inch_mm/2:width]){
            translate([i,0,0])
                notch(notch_width, h_halfway);
        }
        
        //Draw the .1 notches
        for(i = [0:inch_mm*0.1:width]){
            translate([i,0,0])
                notch(notch_width, h_tenths);
        }
        
        //Draw the .02 notches
        for(i = [0:inch_mm*0.02:inch_mm]){
            translate([i,0,0])
                notch(notch_width, h_tiny);
        }
    }
}

module centimeter_labels(width, text_width=0.1, text_height=2, font=default_font){
    
    //union()
    {
        for(i = [0:10:width]){
            translate([i,0,0])
                text(str(i/10), text_height, font);
        }
    }
}

module inch_labels(width, text_width=0.1, text_height=2, font=default_font){
    
    //union()
    {
        for(i = [0:inch_mm:width]){
            translate([i,0,0])
                text(str(i/inch_mm), text_height, font);
        }
    }
}

translate([0,20,0]){
    centimeter(300);
    translate([0,5-2,0]) centimeter_labels(300);
}

translate([0,10,0]){
    inch(300);
    translate([0,5-2,0]) inch_labels(300);
}