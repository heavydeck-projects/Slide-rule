use <include/decade.scad>
use <include/trigonometry.scad>
use <include/linear.scad>

//Width of the scale from origin to maximum (mm)
scale_w = 300;
//Height of the scale markings (mm)
scale_h = 4;
//Separation between scales
scale_margin = scale_h/10;
//Scale label separation (mm)
scale_label_margin = 6.25;
//Width of margins (mm)
margin_w = 25;
//Line width
line_w = 0.1;
//Text height (mm)
text_height = 2;
//Text height on the back of the base (mm)
help_text_height = 4;
//Formula text height
formula_text_height = 2;
//Text height on the scale labelings
label_text_height = scale_h*0.80;

//Offset relative to the 10 side of the rule instead of the 1 side
slide_x_offset_other=false;
//X-Offset of the slide part of the rule (C/D scale)
slide_x_offset=1.0;
slide_x_offset_linear= slide_x_offset_other ? 
    (scale_w * log(slide_x_offset)) - scale_w :
    (scale_w * log(slide_x_offset));

//Font
font_regular = "Liberation Sans";
//Font (alternate)
font_alternate = "Liberation Sans:style=Italic";
//Font (monospaced)
font_mono    = "Liberation Mono";


//Diameter of the screw holes (mm)
drill_size = 3.2; //<-- M3 close fit.

//rendering flags

//Draw the top (fixed) scale
show_top_scale = true;
//Draw the sliding scale
show_slide_scale = true;
//Draw the bottom (fixed) scale
show_bottom_scale = true;
//Draw the base plate
show_base      = true;

//Draw the outline of the pieces
show_outlines  = true;
//Draw the etching/markings
show_engraving = true;
//Draw Fiducials (useful for exporting into SVG/DXF)
show_fiducials = false;





// --- build instructions ---

top_scale_count = 4;
slide_scale_count = 4;
bottom_scale_count = 4;

//Useful calculated constants
board_height = (bottom_scale_count+slide_scale_count+top_scale_count)*(scale_h+scale_margin);
board_width = scale_w + (2*margin_w);
top_scale_height = top_scale_count*(scale_h+scale_margin);
slide_scale_height = slide_scale_count*(scale_h+scale_margin);
bottom_scale_height = bottom_scale_count*(scale_h+scale_margin);

//Screws (as module for easy intersections)
module screws(x_adjust=0){
    drill_fn = 50;
    drill_x_position = 5;
    //One drill at each side of the top scale, 5mm into the wood
    translate([x_adjust + drill_x_position, board_height - (top_scale_height/2), 0])
        circle(d=drill_size, $fn=drill_fn);
    translate([x_adjust + board_width-drill_x_position, board_height - (top_scale_height/2), 0])
        circle(d=drill_size, $fn=drill_fn);
    
    //One drill at each side of the bottom scale, 5mm into the wood
    translate([x_adjust+drill_x_position, bottom_scale_height/2, 0])
        circle(d=drill_size, $fn=drill_fn);
    translate([x_adjust + board_width-drill_x_position, bottom_scale_height/2, 0])
        circle(d=drill_size, $fn=drill_fn);
}

//Top scale:
//  - A / X^2  -- Two-decade log scale
if(show_top_scale)
translate([margin_w, (bottom_scale_count+slide_scale_count)*(scale_h+scale_margin) , 0])
{
    // [1] -- centimeters
    if(show_engraving)
    translate([0,(bottom_scale_count - 1) * (scale_h+scale_margin),0])
    {
        //Notches
        translate([0,(scale_h+scale_margin),0])
        rotate(180, [1,0,0])
        centimeter(scale_w, notch_width=line_w, notch_height=scale_h);
        //Numbers
        translate([0,scale_margin,0]){
            centimeter_labels(scale_w);
        }
        //Label
        {
            //Explanation
            translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
            text("←cm", formula_text_height, font_regular, halign="left", valign="center");
        }
    }
    
    // [2] -- 'T1' scale -- Tangents, up to 45º (Range [0.1 1])
    if(show_engraving)
    translate([0,(top_scale_count - 2) * (scale_h+scale_margin),0])
    {
        //Notches
        tan1_scale(scale_w, notch_width=line_w, notch_height=scale_h);
        //Numbers
        translate([0,scale_h-text_height,0]){
            tan1_labels(scale_w);
        }
        //Label
        {
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("T1", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
            text("←tan(α)", formula_text_height, font_regular, halign="left", valign="center");
        }
    }
    
    // [3] -- 'T2' scale -- Tangents from 45º to atan(10) (Range [1 10])
    if(show_engraving)
    translate([0,(top_scale_count - 3) * (scale_h+scale_margin),0])
    {
        //Notches
        tan2_scale(scale_w, notch_width=line_w, notch_height=scale_h);
        //Numbers
        translate([0,scale_h-text_height,0]){
            tan2_labels(scale_w);
        }
        //Label
        {
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("T2", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
            text("←tan(α)", formula_text_height, font_regular, halign="left", valign="center");
        }
    }
    
    // [4] -- 'A' scale -- Two consecutive log decades.
    if(show_engraving)
    translate([0,(top_scale_count - 4) * (scale_h+scale_margin),0])
    {
        //Notches
        decade(scale_w/2, notch_width=line_w, notch_height=scale_h);
        translate([scale_w/2,0,0])decade(scale_w/2, notch_width=line_w, notch_height=scale_h);
        //Numbers
        translate([0,scale_h-text_height,0]){
            log_scale_labels(scale_w/2, print_one=true);
            translate([scale_w/2,0,0]) log_scale_labels(scale_w/2, end_value=100);
        }
        //Label
        {
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("A", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
            text("←X²", formula_text_height, font_regular, halign="left", valign="center");
        }
    }
    
    //Outline
    if(show_outlines)
    color("DarkGreen") translate([0,0,-1]) difference(){
        polygon([
            [-margin_w,          0],
            [scale_w + margin_w, 0],
            [scale_w + margin_w, top_scale_count*(scale_h+scale_margin)],
            [-margin_w,          top_scale_count*(scale_h+scale_margin)],
        ]);
        screws(-margin_w);
    }
}

//Slide scale:
//  - B / X^2 -- Two-decade log scale
//  - C / X   -- One-decade log scale
if(show_slide_scale)
translate([slide_x_offset_linear,0,0])
translate([margin_w, (bottom_scale_count)*(scale_h+scale_margin) , 0])
{
    
    // [1] 'B' scale -- Two consecutive log decades.
    if(show_engraving)
    translate([0,(slide_scale_count - 1) * (scale_h+scale_margin),0])
    {
        //Notches
        translate([0,(scale_h+scale_margin),0]){
            rotate(180, [1,0,0])decade(scale_w/2, notch_width=line_w, notch_height=scale_h);
            rotate(180, [1,0,0])translate([scale_w/2,0,0])decade(scale_w/2, notch_width=line_w, notch_height=scale_h);
        }
        //Numbers
        translate([0,scale_margin,0]){
            log_scale_labels(scale_w/2, print_one=true);
            translate([scale_w/2,0,0]) log_scale_labels(scale_w/2, end_value=100);
        }
        //Label
        {
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("B", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
            text("←X²", formula_text_height, font_regular, halign="left", valign="center");
        }
    }
    
    // [2] 'BI' scale -- Inverted 2-decade scale
    if(show_engraving)
    translate([scale_w,(slide_scale_count - 2) * (scale_h+scale_margin),0])
    rotate(180, [0,1,0])
    {
        //Notches
        translate([0,(scale_h+scale_margin),0]){
            rotate(180, [1,0,0])decade(scale_w/2, notch_width=line_w, notch_height=scale_h);
            rotate(180, [1,0,0])translate([scale_w/2,0,0])decade(scale_w/2, notch_width=line_w, notch_height=scale_h);
        }
        //Numbers
        translate([0,scale_margin,0]){
            log_scale_labels(scale_w/2, print_one=true, inverse=true);
            translate([scale_w/2,0,0]) log_scale_labels(scale_w/2, end_value=100, inverse=true);
        }
        //Label (undo rotation. I know I know, is Ugly)
        rotate(180, [0,1,0]) translate([-scale_w,0,0])
        {
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("BI", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
            text("←1/X²", formula_text_height, font_regular, halign="left", valign="center");
        }
    }
    
    // [3] 'CI' scale -- Inverted 1-decade scale
    if(show_engraving)
    translate([scale_w,(slide_scale_count - 3) * (scale_h+scale_margin),0])
    rotate(180, [0,1,0])
    {
        //Notches
        {
            decade_precise(scale_w, notch_width=line_w, notch_height=scale_h);
        }
        //Numbers
        translate([0,scale_h-text_height,0]){
            log_scale_labels(scale_w, precise=true, print_one=true, inverse=true);
        }
        //Label (undo rotation. I know I know, is Ugly)
        rotate(180, [0,1,0]) translate([-scale_w,0,0])
        {
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("CI", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
            text("←1/X", formula_text_height, font_regular, halign="left", valign="center");
        }
    }
    
    // [4] 'C' scale -- One high-precision log decade
    if(show_engraving)
    translate([0,(slide_scale_count - 4) * (scale_h+scale_margin),0])
    {
        //Notches
        {
            decade_precise(scale_w, notch_width=line_w, notch_height=scale_h);
        }
        //Numbers
        translate([0,scale_h-text_height,0]){
            log_scale_labels(scale_w, precise=true, print_one=true);
        }
        //Label
        {
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("C", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
            text("←X", formula_text_height, font_regular, halign="left", valign="center");
        }
    }
    
    //Outline
    if(show_outlines)
    color("Green"){
        translate([0,0,-1])
        polygon([
            [-margin_w,          0],
            [scale_w + margin_w, 0],
            [scale_w + margin_w, slide_scale_count*(scale_h+scale_margin)],
            [-margin_w,          slide_scale_count*(scale_h+scale_margin)],
        ]);
    }
}

//Bottom scale:
//  - D / X   -- One-decade log scale
if(show_bottom_scale)
translate([margin_w, 0, 0])
{
    
    // 'D' scale -- One log decades.
    if(show_engraving)
    translate([0,(bottom_scale_count - 1) * (scale_h+scale_margin),0])
    {
        //Notches
        translate([0,(scale_h+scale_margin),0])
        rotate(180, [1,0,0])
        decade_precise(scale_w, notch_width=line_w, notch_height=scale_h);
        //Numbers
        translate([0,scale_margin,0]){
            log_scale_labels(scale_w, print_one=true, precise=true);
        }
        //Label
        {
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("D", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
            text("←X", formula_text_height, font_regular, halign="left", valign="center");
        }
    }
    
    // 'S' scale -- X = sin(S) 
    if(show_engraving)
    translate([0,(bottom_scale_count - 2) * (scale_h+scale_margin),0])
    {
        //Notches
        translate([0,(scale_h+scale_margin),0])
        rotate(180, [1,0,0])
        sine_scale(scale_w, notch_width=line_w, notch_height=scale_h);
        //Numbers
        translate([0,scale_margin,0]){
            sine_labels(scale_w);
            translate([-scale_margin,-scale_margin,0])
                cosine_labels(scale_w);
        }
        //Label
        {
            //Scale name
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("S", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            if(false){ //<-- Explanation in one line
                translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
                text("←sin[cos]", formula_text_height, font_regular, halign="left", valign="center");
            }
            else{ //<-- two lines
                separation = formula_text_height * 0.6;
                translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
                text("←", formula_text_height, font_regular, halign="left", valign="center");
                
                translate([scale_w + scale_label_margin + 3, (scale_h/2)+(scale_margin/2)+separation, 0])
                text("sin(α)", formula_text_height, font_regular, halign="left", valign="center");
                
                translate([scale_w + scale_label_margin + 3, (scale_h/2)+(scale_margin/2)-separation, 0])
                text("cos(α)", formula_text_height, font_alternate, halign="left", valign="center");
            }
        }
    }
    
    // 'ST' scale -- X = sin(ST) && tan(ST) for small angles
    if(show_engraving)
    translate([0,(bottom_scale_count - 3) * (scale_h+scale_margin),0])
    {
        //Notches
        translate([0,(scale_h+scale_margin),0])
        rotate(180, [1,0,0])
        small_sine_scale(scale_w, notch_width=line_w, notch_height=scale_h);
        //Numbers
        translate([0,scale_margin,0]){
            small_sine_labels(scale_w);
        }
        //Label
        {
            //Scale name
            translate([-scale_label_margin,(scale_h/2)+(scale_margin/2),0])
            text("ST", label_text_height, font_regular, halign="right", valign="center");
            
            //Explanation
            {
                translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
                text("←sin(α)", formula_text_height, font_regular, halign="left", valign="center");
            }
        }
    }
    
    // inch scale -- linear scale, in inches
    if(show_engraving)
    translate([0,(top_scale_count - 4) * (scale_h+scale_margin),0])
    {
        //Notches
        inch(scale_w + 7, notch_width=line_w, notch_height=scale_h);
        //Numbers
        translate([0,scale_h-text_height,0]){
            inch_labels(scale_w);
        }
        //Explanation
            {
                translate([scale_w + scale_label_margin, (scale_h/2)+(scale_margin/2), 0])
                text("←inch", formula_text_height, font_regular, halign="left", valign="center");
            }
    }
    
    //Outline
    if(show_outlines)
    color("DarkGreen") translate([0,0,-1]) difference(){
        polygon([
            [-margin_w,          0],
            [scale_w + margin_w, 0],
            [scale_w + margin_w, bottom_scale_count*(scale_h+scale_margin)],
            [-margin_w,          bottom_scale_count*(scale_h+scale_margin)],
        ]);
        screws(-margin_w);
    }
}

//Base
if(show_base)
translate([0,0,-2])
{
    //Outline
    if(show_outlines)
    color("Olive") translate([0,0,-1]) difference(){
        polygon([
            [0          , 0],
            [board_width, 0],
            [board_width, board_height],
            [0          , board_height],
        ]);
        screws();
    }
    
    //Back-of-rule text
    if(show_engraving)
    translate([margin_w,board_height,-2])
    rotate(180, [1,0,0])
    {
        line_count = 7;
        
        //Line 1
        translate([0,board_height - (board_height/line_count) * 1, 0])
            text(
                "00000000011111111112222222222333333333344444444445555555555666666666677777777778888888888999",
                help_text_height, font=font_regular);
        
        //Line 2
        translate([0,board_height - (board_height/line_count) * 2, 0])
            text(
                "12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012",
                help_text_height, font=font_regular);
        
        //Line 3
        translate([0,board_height - (board_height/line_count) * 3, 0])
            text(
                "+------------------------------------------------------------------------------------------+",
                help_text_height, font=font_regular);
        
        //Line 4
        translate([0,board_height - (board_height/line_count) * 4, 0])
            text(
                "+------------------------------------------------------------------------------------------+",
                help_text_height, font=font_regular);
        
        //Line 5
        translate([0,board_height - (board_height/line_count) * 5, 0])
            text(
                "+------------------------------------------------------------------------------------------+",
                help_text_height, font=font_regular);
            
        //Line 6
        translate([0,board_height - (board_height/line_count) * 6, 0])
            text(
                "+------------------------------------------------------------------------------------------+",
                help_text_height, font=font_regular);
        
        //Line 7
        translate([0,board_height - (board_height/line_count) * 7, 0])
            text(
                "+------------------------------------------------------------------------------------------+",
                help_text_height, font=font_regular);
    }
}


//Fiducials
//Used to make exporting to SVG/Other easier
if(show_fiducials)
{
    translate([-10,-10]) square();
    translate([
        board_width+10,
        board_height+10
    ]) square();
}