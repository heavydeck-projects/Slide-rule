
all: svg samples

dirs:
	-mkdir output
	-mkdir output/svg
	-mkdir output/png

clean:
	-rm -r output

svg: dirs
	#generate the markings
	openscad -o output/svg/slide-markings.svg -Dshow_fiducials=true -Dshow_outlines=false -Dshow_base=false -Dshow_slide_scale=true -Dshow_bottom_scale=false -Dshow_top_scale=false -Dshow_engraving=true slide-rule.scad
	openscad -o output/svg/fixed-markings.svg -Dshow_fiducials=true -Dshow_outlines=false -Dshow_base=false -Dshow_slide_scale=false -Dshow_bottom_scale=true -Dshow_top_scale=true -Dshow_engraving=true slide-rule.scad
	#generate the scales
	openscad -o output/svg/slide-rule.svg -Dshow_fiducials=true -Dshow_outlines=true -Dshow_base=false -Dshow_slide_scale=true -Dshow_bottom_scale=false -Dshow_top_scale=false -Dshow_engraving=false slide-rule.scad
	openscad -o output/svg/fixed-rule.svg -Dshow_fiducials=true -Dshow_outlines=true -Dshow_base=false -Dshow_slide_scale=false -Dshow_bottom_scale=true -Dshow_top_scale=true -Dshow_engraving=false slide-rule.scad
	#rem generate cursor
	openscad -o output/svg/cursor-markings.svg -Dshow_fiducials=true -Dshow_outlines=false -Dshow_engraving=true cursor.scad
	openscad -o output/svg/cursor-rule.svg -Dshow_fiducials=true -Dshow_outlines=true -Dshow_engraving=false cursor.scad

#Sample PNG renderings
#  - On non OpenGL2 capable hw, use LIBGL_ALWAYS_SOFTWARE=1 to force
#    Mesa's software-only rendering (slower)
samples: dirs
	openscad -o output/png/sample-00.png --imgsize 900,700 --camera 175,26.4,0,0,0,0,140 --projection=o -Dslide_x_offset=3 slide-rule.scad
