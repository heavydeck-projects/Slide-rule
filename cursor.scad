
// --- WARNING --- WARNING --- WARNING ---
// ---                                 ---
// --- Make sure the dimensions of the ---
// --- cursor match the slide rule one ---
// ---                                 ---
// --- WARNING --- WARNING --- WARNING ---

//Height of the scale markings (mm)
scale_h = 4;
//Separation between scales
scale_margin = scale_h/10;
//Scale count (n)
scale_count = 12;

board_height = scale_count * (scale_h + scale_margin);


// --- End of warning ---

// ------------------------
// --- Cursor arguments ---
// ------------------------

//Width of the cursor (mm)
cursor_w = 40;
//Diameter of the screw holes (mm)
drill_size = 2.2; //<-- M2 close fit.
//Height of the screw-plates above and below the rule
cursor_mounting_width = drill_size*2;
cursor_height = board_height + cursor_mounting_width*2;

//Show top (transparent) layer
show_top = true;
//Show upper spacers
show_upper_spacers=true;
//Show lower spacers
show_lower_spacers=true;
//Show bottom layer
show_bottom=true;

//Show board outlines
show_outlines=true;
//Show board engravings
show_engravings=true;
//Show fiducials
show_fiducials=true;

module cursor_screws(){
    drill_fn = 50;
    drill_x_position = 5;
    //Two drills on top
    translate([drill_x_position, (cursor_mounting_width/2), 0])
        circle(d=drill_size, $fn=drill_fn);
    translate([cursor_w - drill_x_position, (cursor_mounting_width/2), 0])
        circle(d=drill_size, $fn=drill_fn);
    
    //two on bottom
    translate([drill_x_position, cursor_height - (cursor_mounting_width/2), 0])
        circle(d=drill_size, $fn=drill_fn);
    translate([cursor_w - drill_x_position, cursor_height - (cursor_mounting_width/2), 0])
        circle(d=drill_size, $fn=drill_fn);
}

module spacer(){
    color("Olive"){
        scale([cursor_w,cursor_mounting_width,1])
        square();

        translate([0, cursor_mounting_width + board_height, 0])
        scale([cursor_w,cursor_mounting_width,1])
        square();

    }
}

// Top layer (polycarbonate)
if(show_top)
translate([0,0,40]){
    if(show_outlines)
    difference(){
        color([0.2,0.2,0.2], 0.5)
            scale([cursor_w, board_height + 2*cursor_mounting_width, 1])
            square();
        cursor_screws();
    }
    
    if(show_engravings)
    translate([0,0,1])
    {
        translate([cursor_w/2,0])
        scale([0.1,cursor_height,1]) square();
    }
}

//Upper spacers
if(show_upper_spacers && show_outlines)
translate([0,0,30])
difference(){
    spacer();
    cursor_screws();
}

//Lower spacers
if(show_lower_spacers && show_outlines)
translate([0,0,20])
difference(){
    spacer();
    cursor_screws();
}

// bottom layer
if(show_bottom && show_outlines)
translate([0,0,10])
difference(){
    color("Olive")
        scale([cursor_w, board_height + 2*cursor_mounting_width, 1])
        square();
    cursor_screws();
}

if(show_fiducials){
    translate([-10, -10]) square();
    translate([cursor_w + 10, cursor_height + 10]) square();
}